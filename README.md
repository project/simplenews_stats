CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Simplenews Stats is a statistics module for Simplenews. It allows users to
collect information about newsletters that are sent.

For the moment it is a simple version, only the number of clicks and opening
are displayed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/simplenews_stats

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/simplenews_stats


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Simplenews - https://www.drupal.org/project/simplenews


INSTALLATION
------------

 * Install the Simplenews Stats module as you would normally install
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. To create a newsletter, navigate to Administration > Content > Add
       content > Newsletter issue.
    3. After a newsletter has been created, navigate to Administration > Content
       and there is a horizontal tab for Simplenews Stats.


MAINTAINERS
-----------

 * Damien LAGUERRE - https://www.drupal.org/u/damien-laguerre

Supporting Organization:

 * iPika - https://www.drupal.org/ipika
