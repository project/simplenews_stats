<?php

namespace Drupal\simplenews_stats\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by actions.
 *
 * @ingroup simplenews_stats
 *
 * @ViewsFilter("simplenews_stats_action")
 */
class SimplenewsStatsAction extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value'] = [
      '#type' => 'select',
      '#options' => [
        'click' => $this->t('Click'),
        'view' => $this->t('View'),
      ],
    ];
  }

}

