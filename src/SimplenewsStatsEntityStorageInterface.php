<?php


namespace Drupal\simplenews_stats;


use Drupal\Core\Entity\EntityInterface;
use Drupal\simplenews\SubscriberInterface;

interface SimplenewsStatsEntityStorageInterface {

  /**
   * Return the global newsletter stat from related entity (node).
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity use as simplenews.
   *
   * @return \Drupal\simplenews_stats\Entity\SimplenewsStats
   *   The simplenews stats entity.
   */
  public function getFromRelatedEntity(EntityInterface $entity);

  /**
   * Create an entity from subscriber provide by simplenews and the related
   * entity.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The simplenews subscriber.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity use as simplenews.
   *
   * @return \Drupal\simplenews_stats\SimplenewsStatsInterface
   *   The simplenews stats entity.
   */
  public function createFromSubscriberAndEntity(SubscriberInterface $subscriber, EntityInterface $entity);

}
